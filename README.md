# Breeze Dark Theme for Mozilla Firefox

Static theme using WebExtension API. Fully support Breeze colors in every detail.

![Screenshot](screenshot.png)

Tested with:

- Firefox 64.0
- KDE Plasma 5.14.4
- KDE Frameworks 5.53.0
- openSUSE Tumbleweed

## Install

Note: Firefox Addons website currently doesn't support static theme. You cannot
install it by downloading .xpi file, either. You have two ways to install it:

1. Download [manifest.json]('manifest.json'). Go to [about:debugging](about:debugging)
   and click **Load temporary extension** button. Choose the `manifest.json` file
   you downloaded.
2. Install openSUSE RPM package on [OBS](https://software.opensuse.org/download.html?project=home%3Aguoyunhebrave&package=firefox-theme-breeze-dark).

## Configuration

1. Go to Firefox Add-ons and open Themes tab.
2. Enable "Breeze Dark" theme.
3. In KDE System Settings -> Appearance -> Application Style -> GNOME Applications,
   change GTK theme and icon theme to Breeze Dark.

## Webpage Readability

If you enable dark GTK theme, Firefox will use dark background and light text by
default. However, many webpages may have a white background and light text is
almost invisible.

To solve this problem, go to <about:config> page, search `browser.display.use_system_colors`
and change it to `false`.

You can also choose one of these extensions:

1. [Text Contrast for Dark Themes](https://addons.mozilla.org/firefox/addon/text-contrast-for-dark-themes/)
   set text color to black on light background.
2. [Dark Background and Light Text](https://addons.mozilla.org/firefox/addon/dark-background-light-text/)
   force page to use dark background.

## FAQ

**Why Another?**

Most Breeze Dark themes are Light Theme. They can only customize a few colors.
But with WebExtension API, you can almost change all colors.

**Why not use the built-in Dark theme?**

The built-in Dark theme uses a slightly different color than Breeze Dark. This
theme uses exactly the same colors.

## Other Breeze Theme Projects

- [Telegram themes](https://github.com/futpib/breeze-tdesktop-theme) by @futpib
