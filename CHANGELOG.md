# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.1.1] - 2018-09-07

### Changed

- Append domain name to extension ID to be unique

## [1.1.0] - 2018-09-07

### Added

- Extension ID, otherwise you cannot install the theme from .zip file

### Changed

- Changed to static theme type

### Removed

- Removed JavaScript
- Removed color properties that used by older version of Firefox (< 57)

## [1.0.0] - 2018-09-07

### Added

- WebExtension API for Firefox theme
- Breeze Dark colors for all available color properites
